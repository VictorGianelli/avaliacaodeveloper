﻿using System;
using System.Collections.Generic;
using System.Configuration;
//using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AvaliacaoDeveloper
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        string guid = Guid.NewGuid().ToString();
        TextBox txbID = new TextBox();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["placa"] != null)
            {
                txbPlaca.Text = Session["placa"].ToString();
            }
        }

        protected void btSalvar_Click(object sender, EventArgs e)
        {
            int placaId = 0;
            //IncluirPlaca();
            try
            {
                if (txbPlaca.Text != "" && txbRenavam.Text != "" && txbNomeProprietario.Text != "" && txbCpfProprietario.Text != "")
                {
                    //capturar a string de conexao
                    System.Configuration.Configuration rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/MyWebSiteRoot");
                    System.Configuration.ConnectionStringSettings connString;
                    connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConnectionString"];
                    //cria um objeto de conexão
                    SqlConnection con = new SqlConnection();
                    con.ConnectionString = connString.ToString();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "IncluirPlaca";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("placa", txbPlaca.Text);
                    cmd.Parameters.AddWithValue("renavam", txbRenavam.Text);
                    cmd.Parameters.AddWithValue("nomeProprietario", txbNomeProprietario.Text);
                    cmd.Parameters.AddWithValue("cpfProprietario", txbCpfProprietario.Text);

                    if (cbBloqueado.Checked)
                        cmd.Parameters.AddWithValue("bloquear", 1);
                    else
                        cmd.Parameters.AddWithValue("bloquear", 0);
                    //cmd.Parameters.AddWithValue("bloquear", cbBloqueado.Checked.);

                    con.Open();

                    placaId = Convert.ToInt32(cmd.ExecuteScalar());

                    con.Close();

                    lMsg.Text = "O placa " + txbPlaca.Text + " foi inserida com sucesso";
                    //Response.Redirect("~/ListaVeiculos.aspx");
                    txbPlaca.Text = "";
                    txbCpfProprietario.Text = "";
                    txbNomeProprietario.Text = "";
                    txbRenavam.Text = "";
                    cbBloqueado.Checked = false;

                    //txbPlacaId.Text = placaId.ToString();

                }
                else
                {
                    throw new Exception("Algum campo não foi preenchido ainda");
                }
            }
            catch (Exception erro)
            {
                Response.Write("<script> alert('" + erro.Message + "')</script>"); 
                lMsg.Text = "Houve um erro ao inserir o registro";
            }

            //AtualizarPlaca();
            try
            {
                string filepath = Server.MapPath("\\Upload\\"+ guid +"\\");
                //HttpFileCollection uploadedFiles = Request.Files;
                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);
                }

                foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
                {
                    string fileName = Path.GetFileName(postedFile.FileName);
                    string fileExtention = Path.GetExtension(fileName);
                    int fileSize = postedFile.ContentLength;

                    postedFile.SaveAs(Server.MapPath("Upload/" + guid + "/" + fileName));

                    if (fileExtention == ".jpg" || fileExtention == ".jpeg" || fileExtention == ".png")
                    {
                            
                        //capturar a string de conexao
                        System.Configuration.Configuration rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/MyWebSiteRoot");
                        System.Configuration.ConnectionStringSettings connString;
                        connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConnectionString"];
                        //cria um objeto de conexão
                        SqlConnection con = new SqlConnection();
                        con.ConnectionString = connString.ToString();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = con;
                        cmd.CommandText = "AtualizarImageId";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("nome", fileName);
                        cmd.Parameters.AddWithValue("tamanho", fileSize);
                        cmd.Parameters.AddWithValue("imagePath", "Upload/" + guid + "/" + fileName);
                        cmd.Parameters.AddWithValue("Id", placaId);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                        lMsg.Text = "Os dados e imagens foram enviadas com sucesso";

                    }
                    else
                    {
                        throw new Exception("formato de imagem não suportado");
                    }
                }

            }
            catch (Exception erro)
            {
                Response.Write("<script> alert('" + erro.Message + "')</script>"); 
                lMsg.Text = "Houve um erro ao inserir o registro";
            }

        }


    }
}