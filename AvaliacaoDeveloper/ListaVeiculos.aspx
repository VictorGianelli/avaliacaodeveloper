﻿<%@ Page Title="" Language="C#" EnableSessionState="True" MasterPageFile="~/MasterPagePrincipal.Master" AutoEventWireup="true" CodeBehind="ListaVeiculos.aspx.cs" Inherits="AvaliacaoDeveloper.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:TextBox ID="TextBox1" runat="server" BorderWidth="0" Font-Bold="True" Font-Size="XX-Large">Lista de Veículos</asp:TextBox>
    <asp:TextBox ID="TextBox2" runat="server" BorderWidth="0" Font-Size="Medium" Width="176px">Cadastrados no sistema</asp:TextBox>
    <br />
    <hr />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Id" DataSourceID="SqlDataSourceVeiculo" ForeColor="#333333" GridLines="None" OnRowCommand="GridView1_RowCommand" >
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <%--<asp:ButtonField ButtonType="Button" CommandName="btEditar" Text="Visualizar" /> --%>
            <asp:TemplateField HeaderText="Bloqueio">
                  <ItemTemplate>
                      <asp:Image ID="Image1" Height="50px" Width="50px" runat="server" ImageUrl=
                          <%# (Boolean.Parse(Eval("bloquear").ToString())) ? 
                            "~/images/lock.png" 
                            : 
                            "~/images/unlock.png" 
                          %> 
                      />
	              </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="bloquear" HeaderText="bloquear" Visible="False" />
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
            <asp:BoundField DataField="placa" HeaderText="Placa" SortExpression="placa" />
            <asp:BoundField DataField="renavam" HeaderText="Renavam" SortExpression="renavam" />
            <asp:BoundField DataField="nomeProprietario" HeaderText="NomeProprietario" Visible="False" />
            <asp:BoundField DataField="cpfProprietario" HeaderText="cpfProprietario" Visible="false" />
            <asp:HyperLinkField Text="Editar" DataNavigateUrlFields="Id,placa,renavam,nomeProprietario,cpfProprietario,bloquear"
                DataNavigateUrlFormatString="~/VisualizarDados.aspx?Id={0}&placa={1}&renavam={2}&nomeProprietario={3}&cpfProprietario={4}&bloquear={5}&enable=true"/> 
            <asp:HyperLinkField Text="Visualizar" DataNavigateUrlFields="Id,placa,renavam,nomeProprietario,cpfProprietario,bloquear"
                DataNavigateUrlFormatString="~/VisualizarDados.aspx?Id={0}&placa={1}&renavam={2}&nomeProprietario={3}&cpfProprietario={4}&bloquear={5}&enable=false"/>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <EmptyDataTemplate>
            <asp:Image ID="Image1" runat="server" ImageUrl='<%# Bind("bloquear") %>' />
        </EmptyDataTemplate>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSourceVeiculo" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [Veiculos] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Veiculos] ([placa], [renavam], [nomeProprietario], [cpfProprietario]) VALUES (@placa, @renavam, @nomeProprietario, @cpfProprietario)" SelectCommand="SELECT * FROM [Veiculos]" UpdateCommand="UPDATE [Veiculos] SET [placa] = @placa, [renavam] = @renavam, [nomeProprietario] = @nomeProprietario, [cpfProprietario] = @cpfProprietario WHERE [Id] = @Id">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="placa" Type="String" />
            <asp:Parameter Name="renavam" Type="String" />
            <asp:Parameter Name="nomeProprietario" Type="String" />
            <asp:Parameter Name="cpfProprietario" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="placa" Type="String" />
            <asp:Parameter Name="renavam" Type="String" />
            <asp:Parameter Name="nomeProprietario" Type="String" />
            <asp:Parameter Name="cpfProprietario" Type="String" />
            <asp:Parameter Name="Id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
        <asp:Button ID="btCadastrarVeiculo" runat="server" Text="Cadastrar Veículo" OnClick="btCadastrarVeiculo_Click" />
        <br />

</asp:Content>

