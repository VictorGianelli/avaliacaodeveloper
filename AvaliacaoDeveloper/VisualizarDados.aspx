<%@ Page Title="" Language="C#" EnableSessionState="ReadOnly" MasterPageFile="~/MasterPagePrincipal.Master" AutoEventWireup="true" CodeBehind="VisualizarDados.aspx.cs" Inherits="AvaliacaoDeveloper.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="ImageById" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="txbId" DefaultValue="0" Name="imageId" PropertyName="Text" Type="Int32" />
        </SelectParameters>
</asp:SqlDataSource>
    
    <asp:TextBox ID="TextBox1" runat="server" BorderWidth="0" Font-Bold="True" Font-Size="XX-Large">Visualizar Dados</asp:TextBox>
    <asp:TextBox ID="TextBox2" runat="server" BorderWidth="0" Font-Size="Medium">veículo</asp:TextBox><br />
    <hr />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:BoundField DataField="imagePath" HeaderText="nome" SortExpression="nome" Visible="False"/>
            <asp:BoundField DataField="nome" HeaderText="nome" SortExpression="nome" Visible="False" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Bind("imagePath") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <hr />
    <p>
        <asp:TextBox ID="txbId" runat="server" Visible="false" ></asp:TextBox>
        <br />
        <asp:Label ID="Label1" runat="server" Text="Placa" Font-Bold="True"></asp:Label>
        <asp:TextBox ID="txbPlaca" runat="server" Enabled="True" AutoPostback="True" ></asp:TextBox>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Renavam" Font-Bold="True"></asp:Label>
        <asp:TextBox ID="txbRenavam" runat="server" ></asp:TextBox>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Nome do proprietário" Font-Bold="True"></asp:Label>
        <asp:TextBox ID="txbNomeProprietario" runat="server" ></asp:TextBox>
        <br />
        <asp:Label ID="Label4" runat="server" Text="CPF" Font-Bold="True"></asp:Label>
        <asp:TextBox ID="txbCpfProprietario" runat="server"></asp:TextBox>
        <br />
        <asp:CheckBox ID="cbBloqueado" runat="server" />
        <asp:Label ID="Label5" runat="server" Text="Bloqueado" Font-Bold="True"></asp:Label>
        <%--<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>--%>
    </p>
    <br />
    <asp:Button ID="btVoltar" runat="server" Text="Voltar" OnClick="btVoltar_Click" />
    <asp:Button ID="btDeletar" runat="server" Text="Deletar" 
        OnClientClick="return confirm('Você realmente deseja remover esse dado de forma permanente?');" OnClick="btDeletar_Click" />
    
</asp:Content>
