﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AvaliacaoDeveloper
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        static string prevPage = String.Empty;
        //string txtId, txtPlaca, txtRenavam, txtNomeProprietario, txtCpf;

        bool enable, bloqueado;
        //SqlDataAdapter da;
        //DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                prevPage = Request.UrlReferrer.ToString();

                txbId.Text = Request.QueryString["Id"];
                txbPlaca.Text = Request.QueryString["placa"];
                txbRenavam.Text = Request.QueryString["renavam"];
                txbNomeProprietario.Text = Request.QueryString["nomeProprietario"]; ;
                txbCpfProprietario.Text = Request.QueryString["cpfProprietario"];
                cbBloqueado.Checked = Convert.ToBoolean(Request.QueryString["bloquear"]);
            }

            enable = Convert.ToBoolean(Request.QueryString["enable"]);

            if (!enable)
            {
                btDeletar.Visible = false;
                txbPlaca.Enabled = false;
                txbRenavam.Enabled = false;
                txbNomeProprietario.Enabled = false;
                txbCpfProprietario.Enabled = false;
                cbBloqueado.Enabled = false;
            }
            else
            {
                btVoltar.Text = "Atualizar";
            }

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btVoltar_Click(object sender, EventArgs e)
        {
            if (!enable)
            {
                Response.Redirect(prevPage);
            }
            else
            {
                try
                {
                    if (txbPlaca.Text != "" && txbRenavam.Text != "" && txbNomeProprietario.Text != "" && txbCpfProprietario.Text != "")
                    {
                        //capturar a string de conexao
                        System.Configuration.Configuration rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/MyWebSiteRoot");
                        System.Configuration.ConnectionStringSettings connString;
                        connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConnectionString"];
                        //cria um objeto de conexão
                        SqlConnection con = new SqlConnection();
                        con.ConnectionString = connString.ToString();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = con;
                        cmd.CommandText = "AtualizarPlaca";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("Id", txbId.Text);
                        cmd.Parameters.AddWithValue("placa", txbPlaca.Text);
                        cmd.Parameters.AddWithValue("renavam", txbRenavam.Text);
                        cmd.Parameters.AddWithValue("nomeProprietario", txbNomeProprietario.Text);
                        cmd.Parameters.AddWithValue("cpfProprietario", txbCpfProprietario.Text);

                        if (cbBloqueado.Checked)
                            cmd.Parameters.AddWithValue("bloquear", 1);
                        else
                            cmd.Parameters.AddWithValue("bloquear", 0);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();

                    }
                    else
                    {
                        throw new Exception("Algum campo não foi preenchido ainda");
                    }
                }
                catch (Exception erro)
                {
                    Response.Write("<script> alert('" + erro.Message + "')</script>"); //https://youtu.be/9mbEqoM9D2M?t=586
                    //lMsg.Text = "Houve um erro ao inserir o registro";
                }

                Response.Redirect(prevPage);
            }
        }

        protected void btDeletar_Click(object sender, EventArgs e)
        {
            //caixa de confirmação
            try
            {
                if (txbPlaca.Text != "" && txbRenavam.Text != "" && txbNomeProprietario.Text != "" && txbCpfProprietario.Text != "")
                {
                    //capturar a string de conexao
                    System.Configuration.Configuration rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("/MyWebSiteRoot");
                    System.Configuration.ConnectionStringSettings connString;
                    connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConnectionString"];
                    //cria um objeto de conexão
                    SqlConnection con = new SqlConnection();
                    con.ConnectionString = connString.ToString();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "ExcluirPlaca";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("Id", txbId.Text);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                }
                else
                {
                    throw new Exception("Algum campo não foi preenchido ainda");
                }
            }
            catch (Exception erro)
            {
                Response.Write("<script> alert('" + erro.Message + "')</script>"); //https://youtu.be/9mbEqoM9D2M?t=586
                                                                                   //lMsg.Text = "Houve um erro ao inserir o registro";
            }

            Response.Redirect(prevPage);
        }

        protected void txbPlaca_TextChanged(object sender, EventArgs e)
        {
            txbPlaca.Text = sender.ToString();
        }

    }    
}