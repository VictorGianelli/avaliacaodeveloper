Oportunidade de trabalho na Fenox Tecnologia
===========================================

Venha trabalhar na área de tecnologia. Nosso clima é de extrema colaboração. 

Descrição da Vaga
-------------------------------------------------------
Requisitos técnicos

Back-End:
- Microsoft .Net C#
- Framework 3.5 ou Superior
- Conceito de Camadas (BLL, DAO)
- IDE Visual Studio 2015 ou Superior
- Conhecimento em SOLID, Clean Code, Desing Patterns e WCF (Windows Communication Foundation) são diferenciais

Front-End:
- HTML5/CSS, Javascript, Bootstrap 3.x e WebForms (aspx)
- Conhecimento Asp.NET MVC será um diferencial

Banco de Dados:
- SQL Server
- IDE SQL Server Management Studio

Controle de Versão:
- Git

Outros:
- Conhecimento nas ferramentas SoapUI e Postman são diferenciais

Responsabilidades
- Perfil de visão de dono, busca a solução dos problemas, mesmo que dependa de outras pessoas
- Trabalho em equipe 
- Proatividade
- Levantamento de Requisitos
- Não se detém por pequenos problemas, busca contorná-los e encontrar as soluções
- Desejo de apreender cada vez mais e compartilhar conhecimentos

Se você tem interesse em fazer parte de uma equipe multidisciplinar, siga os seguintes passos:
 - Crie um repositório no github ou bitbucket para armazenar a resolução do exercício.
 - Envie um email para rh@fenoxtec.com.br com seu repositório para nós analisarmos a resolução do exercício.


Desafio: Cadastro de Veículos
===========================================

Crie um site SPA que administre um cadastro de veículos em uma base de dados.

O site consiste basicamente em três telas (o layout da tela fica livre para você criar):

 * **Listar os veículos cadastrados** 
   ![Screenshots](lista-01.png)
    
 * **Formulário para cadastro de veículos**
   (O usuário poderá subir um número ilimitado de fotos)
   ![Screenshots](cadastrar.png)
    
 * **Detalhes do veículo** 
   ![Screenshots](visualizar.png)
      
As tecnologias que nós recomendamos a utilizar são as mesmas informadas em nosso requisito técnico da vaga.

Prazos
- 48 horas 

Lembrando que será avaliado a estrutura do código criado, padrões utilizados, testes unitários e qualidade. Não precisa se precoupar muito com o layout. Qualquer dúvida sobre o exercício envie um email para rh@fenoxtec.com.br que responderemos para você!

Boa sorte! =)
