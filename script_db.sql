﻿--Banco de dados da avaliação:
USE MASTER
GO
--Tabelas

CREATE TABLE [dbo].[Fotos] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [nome]      NVARCHAR (255) NOT NULL,
    [tamamho]   INT            NOT NULL,
    [imagePath] NVARCHAR (MAX) NOT NULL,
    [imageId]   INT            DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Veiculos] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [placa]            NVARCHAR (7)  NOT NULL,
    [renavam]          NVARCHAR (11) NOT NULL,
    [nomeProprietario] NVARCHAR (50) NOT NULL,
    [cpfProprietario]  NVARCHAR (11) NOT NULL,
    [bloquear]         BIT           DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

--Procedures

CREATE PROCEDURE [dbo].[AtualizarImageId]
	@nome NVARCHAR(255),
	@tamanho int,
	@imagePath NVARCHAR(MAX),
	@Id int
	
AS
BEGIN
	Insert into Fotos
		values(@nome,@tamanho,@imagePath,@Id);

	SELECT SCOPE_IDENTITY();
END;
GO

CREATE PROCEDURE [dbo].[AtualizarPlaca]
	
	@Id INT,
	@placa VARCHAR(7),
	@renavam VARCHAR(11),
	@nomeProprietario VARCHAR(50),
	@cpfProprietario VARCHAR(11),
	@bloquear INT

AS
BEGIN
	DECLARE @vCadastroId INT;

	SELECT @vCadastroId = @Id;

	UPDATE Veiculos 
		SET placa = @placa, renavam = @renavam, nomeProprietario = @nomeProprietario, cpfProprietario = @cpfProprietario, bloquear = @bloquear
		WHERE Id = @vCadastroId
END;
GO

CREATE PROCEDURE [dbo].[ExcluirPlaca]
	@Id INT
AS
BEGIN
	DECLARE @vCadastroId INT;

	SELECT @vCadastroId = @Id;

	DELETE FROM Veiculos
		WHERE Id = @vCadastroId;

	DELETE FROM Fotos
		WHERE imageId = @vCadastroId;
END;
GO

CREATE PROCEDURE [dbo].[ImageById]
@imageId int
AS
BEGIN
	select nome, imagePath 
	from Fotos 
	where imageId = @imageId;
END;
GO

CREATE PROCEDURE [dbo].[IncluirPlaca]
    @placa VARCHAR(7),
    @renavam VARCHAR(11),
    @nomeProprietario VARCHAR(50),
    @cpfProprietario VARCHAR(11),
    @bloquear INT
AS
BEGIN
    Insert into Veiculos (placa,renavam,nomeProprietario,cpfProprietario,bloquear)
        values (@placa,@renavam,@nomeProprietario,@cpfProprietario,@bloquear);

   Select @@IDENTITY
END;
GO